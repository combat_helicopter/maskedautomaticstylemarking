from time import time
import os
import shutil
import numpy as np
from PIL import Image

from assign_labels import assign_labels
from build_image_hist import get_color_histograms_of_images, color_hist_to_2d


def get_common_mask(masks, th=100):
    masks = np.where(masks > th, 255, 0)
    common_mask = np.max(masks, axis=-1)
    return common_mask > 0


def handler(image_and_masks):
    image_and_masks = [
        img[:, :, None] if len(img.shape) == 2 else img 
        for img in image_and_masks
    ]

    image_and_masks = np.concatenate(image_and_masks, axis=-1)
    return image_and_masks


def make_stargandataset_dir(datasets_path, name, del_prev=False):
    stargan_dataset_path = os.path.join(datasets_path, name)
    os.makedirs(stargan_dataset_path, mode=0o777, exist_ok=True)
    for i in range(1, 12):
        name = 'class_00'
        name_number = str(i)
        name = name[:-len(name_number)] + name_number

        class_path = os.path.join(stargan_dataset_path, name)
        if del_prev & os.path.exists(class_path):
            shutil.rmtree(class_path)
        os.makedirs(class_path, mode=0o777, exist_ok=True)


def save_batch(batch, numbers, labels, dataset_path, 
               name_pattern='000000', class_pattern='class_00'):
    for img, numb, lab in zip(batch, numbers, labels):
        numb = f'{numb}'
        name = name_pattern[:-len(numb)] + numb
        
        class_name = str(lab)
        class_name = class_pattern[:-len(class_name)] + class_name
        img_path = os.path.join(dataset_path, class_name, f'{name}')
        np.save(img_path, img)


def generate_stargan_dataset(reducer, dataset_coroutine, handler, batch_size, stargan_dataset_path):
    images_batch = []
    masks_batch = []
    batch = []

    img_numbers = []
    current_img = 1
    while True:
        try:
            t1 = time()
            for i in range(batch_size):
                image_and_masks = handler(next(dataset_coroutine))

                image = image_and_masks[:, :, :3]
                common_mask = get_common_mask(image_and_masks[:, :, 3:])

                images_batch.append(image)
                masks_batch.append(~common_mask)
                batch.append(image_and_masks)

                img_numbers.append(current_img)
                current_img += 1

            batch_hists = get_color_histograms_of_images(images_batch, masks_batch)
            batch_umap_hists = color_hist_to_2d(batch_hists, reducer)
            labels = assign_labels(batch_umap_hists)

            save_batch(batch, img_numbers, labels, stargan_dataset_path)
            t2 = time()
            print(f'{current_img} images was saved: {round(t2 - t1)}s')

            images_batch = []
            masks_batch = []
            batch = []
            img_numbers = []
        except StopIteration:
            break

    return current_img